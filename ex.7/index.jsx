import React from 'react'
import ReactDOM from 'react-dom'
import ClassComponent from './classComponent'


ReactDOM.render(
    <div>
        <ClassComponent value="componente" />
    </div>    
, document.getElementById("app"))