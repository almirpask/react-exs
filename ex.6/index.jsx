import React from 'react'
import ReactDOM from 'react-dom'
import Family from './family'
import Member from './merber'


ReactDOM.render(
    <div>
        <h1>Memberos da familia</h1>
        <Family lastName="Santos">
            <Member name="Almir" />
            <Member name="Dag" />
            <Member name="Kelly" />
        </Family>
    </div>    
, document.getElementById("app"))